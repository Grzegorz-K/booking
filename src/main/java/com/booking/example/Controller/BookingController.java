package com.booking.example.Controller;

import com.booking.example.Entity.*;
import com.booking.example.Exception.BadRequestException;
import com.booking.example.Exception.ItemNotFoundException;
import com.booking.example.Service.*;
import javassist.NotFoundException;
import javassist.tools.web.BadHttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("booking")
public class BookingController {

    @Autowired
    private MovieService movieService;
    @Autowired
    private ShowService showService;
    @Autowired
    private TicketService ticketService;
    @Autowired
    private BookingService bookingService;
    @Autowired
    private UserService userService;

    @RequestMapping(value="/{movieId}", method = RequestMethod.GET)
    String form(@PathVariable int movieId, Model model) {
        Movie m = movieService.find(movieId);
        List<Show> shows = showService.getCurrentShows(m);
        model.addAttribute("shows", shows);
        model.addAttribute("movie", m);
        return "booking/shows";
    }

    @RequestMapping(value="/show/{showId}", method = RequestMethod.GET)
    String selectTickets( @PathVariable int showId, Model model) throws Exception{
        //Movie m = movieService.find(movieId);
        Show s = showService.find(showId);

        if( s == null) {
            throw new ItemNotFoundException("Show not found");
        }
        Movie m = s.getMovie();
        List<Ticket> tickets = ticketService.findByMovieType(s.getRoom().getMovieType());
        model.addAttribute("show", s);
        model.addAttribute("movie", m);
        model.addAttribute("tickets", tickets);

        return "booking/select-tickets";
    }

    @RequestMapping(value = "/select-seat", method = RequestMethod.POST)
    String selectSeats(@RequestParam(value = "tickets[]") int[] tickets, @RequestParam(value = "show") int showId, Model model, HttpSession session) throws Exception
    {
        if(!ticketService.isTicketCountValid(tickets)) {
            throw new BadRequestException();
        }

        Show s = showService.find(showId);
        List<Ticket> ticketList =  ticketService.findByMovieType(s.getRoom().getMovieType());
        Map<Ticket,Integer> ticketMap = ticketService.bindTicketCount(ticketList,tickets);

        session.removeAttribute("ticketMap");
        session.setAttribute("ticketMap",ticketMap);
        session.setAttribute("showId",showId);

        int sum = 0;

        for (int i : tickets)
            sum += i;

        if(sum <=0) {
            throw new BadRequestException("No tickets selected");
        }

        model.addAttribute("ticketCount", sum);
        model.addAttribute("price", ticketService.countPrice(ticketMap));
        model.addAttribute("show", showService.find(showId));
        return "booking/select-seats";
    }

    @RequestMapping(value = "booked-seats/{showId}",  method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    Set<Seat> booked( @PathVariable int showId, Model model)
    {
        Show show = showService.find(showId);
        Set<Seat> set = bookingService.getAllShowBookedSeats(show);



        return set;
    }

    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    String checkout(@RequestParam(value = "tickets[]") int[] seats, Model model, HttpSession session, HttpServletRequest request)
    {
        Show show = showService.find((int)session.getAttribute("showId"));
        @SuppressWarnings("unchecked")
        Map<Ticket,Integer> tickets = (Map<Ticket,Integer>) session.getAttribute("ticketMap");

        Set<Seat> seatsSet = new HashSet<>();
        for(int x :seats) {
            seatsSet.add(new Seat(x/100, x%100));
        }
        Booking booking = new Booking();
        booking.setSeats(seatsSet);
        booking.setShow(show);

        String username = request.getUserPrincipal().getName();
        User u = userService.findByUsername(username);
        booking.setUser(u);

        bookingService.save(booking);

        model.addAttribute("show",show);
        return "booking/confirm";
    }
}
