package com.booking.example.Controller;

import com.booking.example.Entity.Movie;
import com.booking.example.Service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("movies")
public class MovieController {

    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @RequestMapping(value="", method = RequestMethod.GET)
    String movies(Model model) {
        model.addAttribute("movies", movieService.getAll());
        return "admin/movies";
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    String movie(@PathVariable int id, Model model) {
        model.addAttribute("movie", movieService.find(id));
        return "admin/movie-form";
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    String movie(Model model) {
        model.addAttribute("movie", new Movie());
        return "admin/movie-form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveMovie(@Valid Movie movie, BindingResult bindingResult, final RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "admin/movie-form";
        }

        movieService.save(movie);
        if (movie.getImageFile() != null) {
            movieService.updateImage(movie);
            movieService.save(movie);
        }
        redirectAttributes.addFlashAttribute("message", "Changes saved successfully.");
        return "redirect:/movies/" + movie.getId();
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable Integer id, Model model) {
        movieService.delete(id);
        return "redirect:/movies";
    }

}
