package com.booking.example.Controller;

import com.booking.example.Entity.Room;
import com.booking.example.Service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("rooms")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @RequestMapping(value="", method = RequestMethod.GET)
    String all(Model model) {
        model.addAttribute("rooms", roomService.getAll());
        return "admin/rooms";
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    String form(@PathVariable int id, Model model) {
        model.addAttribute("room", roomService.find(id));
        return "admin/room-form";
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    String newroom( Model model) {
        model.addAttribute("room", new Room());
        return "admin/room-form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveMovie(@Valid Room room, BindingResult bindingResult, final RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "admin/movie-form";
        }
        roomService.save(room);

        redirectAttributes.addFlashAttribute("message", "Changes saved successfully.");
        return "redirect:/rooms/" + room.getId();
    }

}
