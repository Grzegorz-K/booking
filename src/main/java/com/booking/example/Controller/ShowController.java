package com.booking.example.Controller;

import com.booking.example.Entity.Show;
import com.booking.example.Service.MovieService;
import com.booking.example.Service.RoomService;
import com.booking.example.Service.ShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("shows")
public class ShowController {
    @Autowired
    private ShowService showService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private RoomService roomService;

    @RequestMapping(value="", method = RequestMethod.GET)
    String all(Model model) {
        model.addAttribute("shows", showService.getAll());
        return "admin/shows";
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    String form(@PathVariable int id, Model model) {
        model.addAttribute("show", showService.find(id));
        model.addAttribute("movies", movieService.getAll());
        model.addAttribute("rooms", roomService.getAll());
        return "admin/show-form";
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    String newroom( Model model) {
        model.addAttribute("show", new Show());
        model.addAttribute("movies", movieService.getAll());
        model.addAttribute("rooms", roomService.getAll());
        return "admin/show-form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveMovie(@Valid Show show, BindingResult bindingResult, final RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "admin/show-form";
        }
        showService.save(show);

        redirectAttributes.addFlashAttribute("message", "Changes saved successfully.");
        return "redirect:/shows/" + show.getId();
    }
}
