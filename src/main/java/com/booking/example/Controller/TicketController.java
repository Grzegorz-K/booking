package com.booking.example.Controller;

import com.booking.example.Entity.Ticket;
import com.booking.example.Service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("tickets")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @RequestMapping(value="", method = RequestMethod.GET)
    String all(Model model) {
        model.addAttribute("tickets", ticketService.getAll());
        return "admin/tickets";
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    String form(@PathVariable int id, Model model) {
        model.addAttribute("ticket", ticketService.find(id));
        return "admin/ticket-form";
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    String newroom( Model model) {
        model.addAttribute("ticket", new Ticket());
        return "admin/ticket-form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveMovie(@Valid Ticket ticket, BindingResult bindingResult, final RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "admin/ticket-form";
        }
        ticketService.save(ticket);

        redirectAttributes.addFlashAttribute("message", "Changes saved successfully.");
        return "redirect:/tickets/" + ticket.getId();
    }

}
