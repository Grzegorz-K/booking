package com.booking.example.Controller;


import com.booking.example.Entity.User;
import com.booking.example.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value="", method = RequestMethod.GET)
    String all(Model model) {
        model.addAttribute("users", userService.getAll());
        return "admin/users";
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    String form(@PathVariable int id, Model model) {
        model.addAttribute("formUser", userService.find(id));
        return "admin/user-form";
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    String newroom( Model model) {
        model.addAttribute("formUser", new User());
        return "admin/user-form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveMovie(@Valid User user, BindingResult bindingResult, final RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "admin/user-form";
        }
        userService.save(user);

        redirectAttributes.addFlashAttribute("message", "Changes saved successfully.");
        return "redirect:/users/" + user.getId();
    }

}
