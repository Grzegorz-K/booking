package com.booking.example.Entity;


import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "movies")
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="movie_id")
    private int movieId;

    @NotNull
    @Size(min = 2, max = 96)
    private String title;

    @Min(1)
    private int length;

    @NotNull
    @ElementCollection
    @Enumerated(EnumType.ORDINAL)
    private Set<MovieType> types;

    @Transient
    private MultipartFile imageFile;

    private String image;

    public Set<MovieType> getTypes() {
        return types;
    }

    public void setTypes(Set<MovieType> type) {
        this.types = type;
    }

    public int getId() {
        return movieId;
    }

    public void setId(int movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public MultipartFile getImageFile() {
        return imageFile;
    }

    public void setImageFile(MultipartFile imageFile) {
        this.imageFile = imageFile;
    }

    @Override
    public String toString() {
        return "Movie{" + "movieId=" + movieId + ", title='" + title + '\'' + ", length=" + length + '}';
    }
}
