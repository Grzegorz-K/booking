package com.booking.example.Entity;

public enum MovieType {

    MOVIE_2D("2D"),
    MOVIE_3D("3D"),
    MOVIE_IMAX("IMAX");

    private final String displayName;

    MovieType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
