package com.booking.example.Entity;

public enum Role {
    ROLE_USER("User"), ROLE_WORKER("Worker"), ROLE_ADMIN("Admin");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
