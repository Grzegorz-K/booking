package com.booking.example.Entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="shows")
public class Show {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="show_id")
    private int id;

    @ManyToOne
    @JoinColumn (name="movie_id")
    private Movie movie;

    @ManyToOne
    @JoinColumn (name="room_id")
    private Room room;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }


    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
