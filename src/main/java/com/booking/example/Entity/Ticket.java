package com.booking.example.Entity;

import javax.persistence.*;

@Entity
@Table(name="tickets")
public class Ticket
{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="ticket_id")
    private int id;

    private String name;

    private double price;

    private MovieType movieType;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public void setMovieType(MovieType movieType) {
        this.movieType = movieType;
    }

    @Override
    public String toString() {
        return "Ticket{" + "id=" + id + ", name='" + name + '\'' + ", price=" + price + ", movieType=" + movieType + '}';
    }
}
