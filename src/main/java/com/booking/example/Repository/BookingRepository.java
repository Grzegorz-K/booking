package com.booking.example.Repository;

import com.booking.example.Entity.Booking;
import com.booking.example.Entity.Movie;
import com.booking.example.Entity.Seat;
import com.booking.example.Entity.Show;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface BookingRepository extends JpaRepository<Booking,Integer>{

    List<Booking> findByShow(@Param("show") Show show);

    @Query("SELECT CASE WHEN COUNT(b) > 0 THEN false ELSE true END from Booking b where b.show = :show and :seat member of b.seats")
    boolean isSeatAvailable(@Param("show") Show show,  @Param("seat")Seat seat);




//    @Query(value = "Select b.user from Booking b where b.show = :showId")
//    List<Object[]> getShowSeats(@Param("showId") int showId);
}
