package com.booking.example.Repository;

import com.booking.example.Entity.Movie;
import com.booking.example.Entity.Seat;
import com.booking.example.Entity.Show;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface ShowRepository extends JpaRepository<Show,Integer>{

    List<Show> findByMovie(@Param("movie") Movie movie);


    List<Show> findByMovieAndTimeGreaterThanOrderByTime(Movie m, Date time);


}
