package com.booking.example.Repository;

import com.booking.example.Entity.MovieType;
import com.booking.example.Entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Integer>{

    List<Ticket> findByMovieType(@Param("movieType") MovieType movieType);
}
