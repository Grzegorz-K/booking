package com.booking.example.Service.Auth;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}