package com.booking.example.Service;

import com.booking.example.Entity.Booking;
import com.booking.example.Entity.Seat;
import com.booking.example.Entity.Show;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface BookingService {

    Set<Seat> getAllShowBookedSeats(Show show);

    boolean save(Booking booking);
}
