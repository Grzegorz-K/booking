package com.booking.example.Service;

import com.booking.example.Entity.Booking;
import com.booking.example.Entity.Seat;
import com.booking.example.Entity.Show;
import com.booking.example.Repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MainBookingService implements BookingService{
    @Autowired
    private BookingRepository bookingRepository;

    @Override
    public Set<Seat> getAllShowBookedSeats(Show show) {
        List<Booking> bookings =  bookingRepository.findByShow(show);

        Set<Seat> seats = new HashSet<>();
        for (Booking b: bookings) {
            seats.addAll(b.getSeats());
        }

        return seats;
    }

    @Override
    @Transactional
    public boolean save(Booking booking) {

        Set<Seat> seats = booking.getSeats();
        for(Seat s: seats) {
            if(!bookingRepository.isSeatAvailable(booking.getShow(), s)) {
                return false;
            }
        }
        bookingRepository.save(booking);
        return true;
    }
}
