package com.booking.example.Service;

import com.booking.example.Entity.Movie;
import com.booking.example.Repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;

@Service
public class MainMovieService implements MovieService {

    private final MovieRepository movieRepository;

    private final String IMAGES_DIR = "/images";
    private final Path rootLocation = Paths.get("src/main/resources" + IMAGES_DIR);

    @Autowired
    public MainMovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }


    @Override
    public List<Movie> getAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie find(int id) {
        return movieRepository.findOne(id);
    }

    @Override
    public Movie save(Movie m) {
        return movieRepository.save(m);
    }

    @Override
    public void delete(int id) {
        Movie m = movieRepository.findOne(id);

        if(m != null && m.getImage() != null)
        {
            String filename = m.getImage().split("\\?")[0];
            filename = filename.split("images/")[1];
            try {
                Files.delete(this.rootLocation.resolve(filename));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        movieRepository.delete(id);
    }

    @Override
    public void updateImage(Movie m) {
        MultipartFile file = m.getImageFile();

        if((m.getImage() == null || !m.getImage().equals(file.getOriginalFilename())) && !file.getOriginalFilename().isEmpty()) {
            try {
                String originalFilename = file.getOriginalFilename();
                String extension =  originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
                String filename =  "movie_" + m.getId() + "." + extension;
                Files.copy(file.getInputStream(), this.rootLocation.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);

                m.setImage(IMAGES_DIR + "/" + filename + "?" + new Date().getTime());

                m.setImageFile(null);


            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            m.setImage(m.getImage());
        }
    }
}
