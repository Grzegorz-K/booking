package com.booking.example.Service;

import com.booking.example.Entity.Room;
import com.booking.example.Repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MainRoomService implements RoomService {
    @Autowired
    private RoomRepository roomRepository;

    @Override
    public List<Room> getAll() {
        return roomRepository.findAll();
    }

    @Override
    public Room find(int id) {
        return roomRepository.findOne(id);
    }

    @Override
    public Room save(Room r) {
        return roomRepository.save(r);
    }

    @Override
    public void delete(int id) {
        roomRepository.delete(id);
    }
}
