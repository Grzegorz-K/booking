package com.booking.example.Service;

import com.booking.example.Entity.Movie;
import com.booking.example.Entity.Seat;
import com.booking.example.Entity.Show;
import com.booking.example.Repository.ShowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class MainShowService implements ShowService{
    @Autowired
    private ShowRepository showRepository;

    @Override
    public List<Show> getAll() {
        return showRepository.findAll();
    }

    @Override
    public Show find(int id) {
        return showRepository.findOne(id);
    }

    @Override
    public Show save(Show s) {
        return showRepository.save(s);
    }

    @Override
    public void delete(int id) {
        showRepository.delete(id);
    }

    @Override
    public List<Show> findByMovie(Movie m) {
        return showRepository.findByMovie(m);
    }

    @Override
    public List<Show> getCurrentShows(Movie m) {
        return showRepository.findByMovieAndTimeGreaterThanOrderByTime(m,new Date());
    }

    @Override
    public Set<Seat> getAllShowBookedSeats(Show show) {
        return null;
    }
}
