package com.booking.example.Service;

import com.booking.example.Entity.MovieType;
import com.booking.example.Entity.Ticket;
import com.booking.example.Repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MainTicketService implements TicketService{

    @Autowired
    TicketRepository ticketRepository;

    @Override
    public List<Ticket> getAll() {
        return ticketRepository.findAll();
    }

    @Override
    public List<Ticket> findByMovieType(MovieType type) {
        return ticketRepository.findByMovieType(type);
    }

    @Override
    public Ticket find(int id) {
        return ticketRepository.findOne(id);
    }

    @Override
    public Ticket save(Ticket t) {
        return ticketRepository.save(t);
    }

    @Override
    public void delete(int id) {
        ticketRepository.delete(id);
    }

    @Override
    public Map<Ticket, Integer> bindTicketCount(List<Ticket> ticketList, int[] tickets) {
        Map<Ticket,Integer> binded = new HashMap<>();

        for(int i=0; i < tickets.length; i++)
        {
            binded.put(ticketList.get(i),tickets[i]);
        }
        return binded;
    }

    @Override
    public int countPrice(Map<Ticket, Integer> tickets) {

        int price = 0;
        for(Map.Entry<Ticket,Integer> entry: tickets.entrySet())
        {
            price += entry.getValue() * entry.getKey().getPrice();
        }
        return price;
    }

    @Override
    public boolean isTicketCountValid(int[] tickets) {
        for(int x: tickets) {
            if(x < 0)
                return false;
        }
        return true;
    }
}
