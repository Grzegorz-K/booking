package com.booking.example.Service;

import com.booking.example.Entity.Movie;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MovieService {

    List<Movie> getAll();

    Movie find(int id);

    Movie save(Movie m);

    void delete(int id);

    void updateImage(Movie m);


}
