package com.booking.example.Service;

import com.booking.example.Entity.Room;

import java.util.List;

public interface RoomService {

    List<Room> getAll();

    Room find(int id);

    Room save(Room r);

    void delete(int id);
}
