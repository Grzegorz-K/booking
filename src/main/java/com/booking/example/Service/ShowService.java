package com.booking.example.Service;

import com.booking.example.Entity.Movie;
import com.booking.example.Entity.Seat;
import com.booking.example.Entity.Show;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface ShowService {
    List<Show> getAll();

    List<Show> findByMovie(Movie m);

    List<Show> getCurrentShows(Movie m);

    Show find(int id);

    Show save(Show s);

    void delete(int id);

    Set<Seat> getAllShowBookedSeats(Show show);

}
