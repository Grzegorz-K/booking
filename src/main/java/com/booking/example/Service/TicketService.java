package com.booking.example.Service;

import com.booking.example.Entity.MovieType;
import com.booking.example.Entity.Ticket;

import java.util.List;
import java.util.Map;

public interface TicketService {
    List<Ticket> getAll();

    List<Ticket> findByMovieType(MovieType type);

    Ticket find(int id);

    Ticket save(Ticket t);

    void delete(int id);

    boolean isTicketCountValid(int[] tickets);

    Map<Ticket, Integer> bindTicketCount(List<Ticket> ticketList, int[] tickets);

    int countPrice(Map<Ticket, Integer> tickets);

}
