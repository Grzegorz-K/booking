package com.booking.example.Service;

import com.booking.example.Entity.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User find(int id);


    void save(User user);

    User findByUsername(String username);
}
